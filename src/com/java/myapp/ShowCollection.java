/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.java.myapp;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import java.util.Set;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JTextPane;

/**
 *
 * @author ASUS
 */
public class ShowCollection extends javax.swing.JFrame {

    /**
     * Creates new form ShowDisplayAllDBandCollections
     */
    public ShowCollection() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblshowDB = new javax.swing.JLabel();
        btnShowDB = new javax.swing.JButton();
        lblShowCollections = new javax.swing.JLabel();
        btnShowCollections = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        showAllDB = new javax.swing.JList<>();
        jScrollPane4 = new javax.swing.JScrollPane();
        showCollection = new javax.swing.JList<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Show Collection Name");

        lblshowDB.setFont(new java.awt.Font("Microsoft Sans Serif", 1, 16)); // NOI18N
        lblshowDB.setText("1. ติดต่อฐานข้อมูล และ แสดงฐานข้อมูลในระบบ");

        btnShowDB.setFont(new java.awt.Font("Microsoft Sans Serif", 0, 12)); // NOI18N
        btnShowDB.setText("แสดงฐานข้อมูลทั้งหมด");
        btnShowDB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShowDBActionPerformed(evt);
            }
        });

        lblShowCollections.setFont(new java.awt.Font("Microsoft Sans Serif", 1, 16)); // NOI18N
        lblShowCollections.setText("2. แสดงข้อมูล Collection ทั้งหมด");

        btnShowCollections.setFont(new java.awt.Font("Microsoft Sans Serif", 0, 12)); // NOI18N
        btnShowCollections.setText("แสดง Collection");
        btnShowCollections.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShowCollectionsActionPerformed(evt);
            }
        });

        btnExit.setFont(new java.awt.Font("Microsoft Sans Serif", 1, 14)); // NOI18N
        btnExit.setText("Exit");
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        jScrollPane3.setViewportView(showAllDB);

        jScrollPane4.setViewportView(showCollection);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblshowDB)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnShowDB)
                            .addComponent(btnShowCollections, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(85, 85, 85)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(87, 87, 87)
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(lblShowCollections, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(59, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnExit)
                .addGap(30, 30, 30))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(lblshowDB)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnShowDB))
                .addGap(29, 29, 29)
                .addComponent(lblShowCollections)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnShowCollections)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(39, 39, 39)
                .addComponent(btnExit)
                .addContainerGap(41, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    
    private void btnShowCollectionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShowCollectionsActionPerformed
        try {
                 MongoClient mongo = new MongoClient("localhost", 27017);
                 String value = showAllDB.getSelectedValue().toString(); //ดึงข้อมูลที่กดเลือก
                 DB db = mongo.getDB(value); //สร้างตัวแปรเพื่อเข้าไปจัดการฐานข้อมูล
                 Set<String> tables = db.getCollectionNames(); //ดึงชื่อ collection มาเก็บไว้ในตัวแปร
                 DefaultListModel listColl = new DefaultListModel();
                 for(String coll : tables){
	    listColl.addElement(coll);
                  }     
                   showCollection.setModel(listColl);
        } catch (Exception e) {
      }

    }//GEN-LAST:event_btnShowCollectionsActionPerformed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        setVisible(false);
    }//GEN-LAST:event_btnExitActionPerformed

    private void btnShowDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShowDBActionPerformed
        try {
                 MongoClient mongo = new MongoClient("localhost", 27017);
                 List<String> dbs = mongo.getDatabaseNames();
                 DefaultListModel listDB = new DefaultListModel();
                 for(String db : dbs){
                     listDB.addElement(db);
                 }
                 showAllDB.setModel(listDB);
          
     } catch (Exception e) {
     }

    }//GEN-LAST:event_btnShowDBActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ShowCollection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ShowCollection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ShowCollection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ShowCollection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ShowCollection().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnShowCollections;
    private javax.swing.JButton btnShowDB;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lblShowCollections;
    private javax.swing.JLabel lblshowDB;
    private javax.swing.JList<String> showAllDB;
    private javax.swing.JList<String> showCollection;
    // End of variables declaration//GEN-END:variables
}
